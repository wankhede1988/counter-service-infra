resource "aws_ecr_repository" "registry" {
  name                 = "gw-counter-service-ecr"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}