//provider "aws" {
  //region     = "ap-south-1"
  //access_key = "AKIASQKHJFK5C3CPNHDM"
  ///secret_key = "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"
//}

provider "aws" {
    profile =                 "default"
    shared_credentials_file = "~/.aws/test_task"
    region =                  "ap-south-1"
    default_tags {
      tags = {
        Owner        = "ganesh_wankhede"
      }
    }
}

//========== storing tfstae to s3 bucket
terraform {
  backend "s3" {
    profile = "default"
    access_key = "AKIASQKHJFK5C3CPNHDM"
    secret_key = "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"

    bucket = "gw-tf-state"
    key    = "terraform.tfstate"
    region = "ap-south-1"

    dynamodb_table = "gw-dynamodb"
    encrypt        = true
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}