module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "gw-gitlab-server"

  ami                    = "ami-041d6256ed0f2061c"
  instance_type          = "t3a.xlarge"
  key_name               = "gw-gitlab"
  //monitoring             = true
  vpc_security_group_ids = [module.ssh_security_group.security_group_id,module.http_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  associate_public_ip_address = true

  user_data = <<-EOT
  #!/bin/bash -xe
  exec > >(tee /var/log/cloud-init-output.log |logger -t user-data -s 2>/dev/console) 2>&1
  sudo yum update -y
  curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
  sudo yum install -y gitlab-ee
  sudo  gitlab-ctl reconfigure
  sudo cat /etc/gitlab/initial_root_password
  sudo iptables -F
  EOT

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp3"
      volume_size = 5
      throughput  = 200
    }
  ]

  tags = {
      desc    = "used for gitlab"
    }
}


module "ssh_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 4.0"

  name        = "gw-gitlab-SSH"
  description = "Security group for web-server with SSH ports open "
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["163.53.201.131/32"]
  ingress_rules       = ["ssh-tcp"]
}


module "http_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "gw-gitlab-http"
  description = "Security group for web-server with HTTP ports open "
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["163.53.201.131/32"]
  ingress_rules       = ["http-80-tcp"]
}
